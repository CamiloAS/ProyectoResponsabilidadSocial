using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUIManager : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Stories");
    }

        public void Storie1()
    {
        SceneManager.LoadScene("Game1");
    }

        public void Storie2()
    {
        SceneManager.LoadScene("Game2");
    }

        public void Storie3()
    {
        SceneManager.LoadScene("Game3");
    }

        public void Storie4()
    {
        SceneManager.LoadScene("Game4");
    }


    public void QuiteGame()
    {
        Application.Quit();
    }

}
