using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TextDescription : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject HistoryDescription;

    void Start()
    {
        HistoryDescription.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        HistoryDescription.SetActive(true);
    }

    public void OnPointerExit(PointerEventData pointerEventData) 
    {
         HistoryDescription.SetActive(false);
    }
}
